Golly
=

Golly is simulator based on Conway's Game of Life

Setup
-
- install composer dependencies
```
$ composer install
```
- put input and settings to file "in.yml" (demo is included)

Run
-
- run "index.php"
- output is saved to file "out.yml"

Recommendations
-
- do not set the "cells" and "iterations" parameters to high, it might slow down the solution

Possible improvements
-
- killswitch = stop iterate after all cells are dead or in semi/stable state (planned in v2)