<?php

namespace golly;

class Cell{
    /**
     * @var Cell[]
     */
    private $neighbors = [];

    /**
     * @var int
     */
    private $organism = null;

    /**
     * @var int[]
     */
    private $nextIterationOrganisms = [];

    /**
     * @param Cell $cell
     */
    public function addNeighbor(Cell $cell)
    {
        if(!in_array($cell, $this->neighbors, true)){
            $this->neighbors[] = $cell;
        }
    }

    /**
     * @return int|null
     */
    public function getOrganism():? int
    {
        return $this->organism;
    }

    /**
     * @param int $organism
     */
    public function setOrganism(int $organism)
    {
        $this->organism = $organism;
    }

    /**
     * @param int $organism
     */
    public function addOrganismToNextIteration(int $organism)
    {
        $this->nextIterationOrganisms[] = $organism;
    }

    /**
     * Two organisms can't occupy one cell, let one random die
     * @throws \Exception
     * @return void
     */
    public function solveSchism()
    {
        $this->organism = null;
        if (count($this->nextIterationOrganisms) > 0) {
            $rand = array_rand($this->nextIterationOrganisms);
            $this->organism = $this->nextIterationOrganisms[$rand];
        }
    }

    /**
     * Check rules, prepare for next iteration
     * @return void
     */
    public function prepareIteration()
    {
        $this->nextIterationOrganisms = [];

        $organisms = [];
        foreach ($this->neighbors as $neighbor) {
            $organisms[] = $neighbor->getOrganism();
        }
        $organisms = array_filter($organisms);
        $organismsCountByType = array_count_values($organisms);

        //check survive
        if (!is_null($this->organism)
            && isset($organismsCountByType[$this->organism])
            && $organismsCountByType[$this->organism] >= 2
            && $organismsCountByType[$this->organism] <= 3
        ) {
            $this->addOrganismToNextIteration($this->organism);
            return;
        }

        //check birth
        foreach ($organismsCountByType as $type => $count) {
            if ($count == 3) {
                $this->addOrganismToNextIteration($type);
            }
        }
    }
}
