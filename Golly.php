<?php

namespace golly;

use Symfony\Component\Yaml\Yaml;

class Golly{

    /**
     * @var int World width (height)
     */
    private $worldWidth = 40;

    /**
     * @var int Iterations count
     */
    private $iterationsCount = 1;

    /**
     * @var array
     */
    private $initialOrganisms = [];

    /**
     * @var Cell[][]
     */
    private $world = [];

    /**
     * @var Cell[]
     */
    private $cells = [];

    /**
     * Golly constructor.
     */
    public function __construct()
    {
        $input = Yaml::parse(file_get_contents('in.yml'));
        $this->worldWidth = $input['world']['cells'];
        $this->iterationsCount = $input['world']['iterations'];
        foreach ($input['organisms'] as $organism) {
            $this->initialOrganisms[] = [$organism['x'], $organism['y'], $organism['species']];
        }
    }

    /**
     * @param $row
     * @param $col
     * @return Cell|null
     */
    private function getCell($row, $col):? Cell
    {
        return $this->world[$row][$col] ?? null;
    }

    /**
     * @param Cell $cell
     * @param $row
     * @param $col
     */
    private function addNeighbor(Cell $cell, $row, $col)
    {
        $neighbor = $this->getCell($row, $col);
        if ($neighbor instanceof Cell) {
            $cell->addNeighbor($neighbor);
        }
    }

    /**
     * Create interconnected cells of world
     * @return void
     */
    public function createWorld()
    {
        // create cells
        for ($row=1; $row<=$this->worldWidth; $row++) {
            $this->world[$row] = [];
            for ($col=1; $col<=$this->worldWidth; $col++) {
                $cell = new Cell();
                $this->world[$row][$col] = $cell;
                $this->cells[] = $cell;
            }
        }

        // interconnect cells
        foreach ($this->world as $row => $cols) {
            foreach ($cols as $col => $cell) {
                /** @var Cell $cell */
                $this->addNeighbor($cell, $row-1, $col-1);
                $this->addNeighbor($cell, $row-1, $col);
                $this->addNeighbor($cell, $row-1, $col+1);
                $this->addNeighbor($cell, $row, $col-1);
                $this->addNeighbor($cell, $row, $col+1);
                $this->addNeighbor($cell, $row+1, $col-1);
                $this->addNeighbor($cell, $row+1, $col);
                $this->addNeighbor($cell, $row+1, $col+1);
            }
        }
    }

    /**
     * Populate organisms from input = generation 0
     * Solve schism
     * @throws \Exception
     * @return void
     */
    public function initialPopulate()
    {
        // populate initial
        foreach ($this->initialOrganisms as $organism) {
            $this->getCell($organism[0], $organism[1])->addOrganismToNextIteration($organism[2]);
        }

        // solve initial conflicts
        $this->solveSchism();
    }

    /**
     * Iterate world
     * @throws \Exception
     * @return void
     */
    public function iterate()
    {
        for ($i=1;$i<=$this->iterationsCount;$i++) {
            // prepare iteration
            foreach ($this->cells as $cell) {
                $cell->prepareIteration();
            }
            // solve schism
            $this->solveSchism();
        }
    }

    /**
     * Solve schism for each cell
     * @throws \Exception
     * @return void
     */
    private function solveSchism()
    {
        foreach ($this->cells as $cell) {
            $cell->solveSchism();
        }
    }

    /**
     * Create output format and save file
     * @return void
     */
    public function output()
    {
        $output = [];
        $output['world']['cells'] = $this->worldWidth;

        $species = [];
        foreach ($this->world as $row => $cols) {
            foreach ($cols as $col => $cell) {
                /** @var Cell $cell */
                if (!is_null($cell->getOrganism())) {
                    $species[] = $cell->getOrganism();
                    $output['organisms'][] = [
                        'x' => $row,
                        'y' => $col,
                        'species' => $cell->getOrganism()
                    ];
                }
            }
        }

        $output['world']['iterations'] = $this->iterationsCount;

        $output['world']['species'] = count(
            array_unique(
                array_filter($species)
            )
        );

        $yaml = Yaml::dump($output);
        file_put_contents('out.yml', $yaml);
    }
}
